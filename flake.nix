{
  inputs = {
    nixpkgs.follows = "haskell-nix/nixpkgs-unstable";
    haskell-nix.url = "github:input-output-hk/haskell.nix";
    flake-utils.url = "github:numtide/flake-utils";
    napalm.url = "github:nmattia/napalm";
  };

  outputs = { self, nixpkgs, flake-utils, haskell-nix, napalm }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "webapp-template";
        version = "1.0.0";
        cabal-index-state = "2021-06-06T00:00:00Z";

        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            napalm.overlay
            haskell-nix.overlay
            (self: super: {
              backend = self.haskell-nix.project' {
                src = ./backend;
                compiler-nix-name = "ghc8104";
                index-state = cabal-index-state;
                configureArgs = "-O2";
                modules = [{
                  packages.backend.components = {
                    library = { dontStrip = false; };
                    exes.server = { dontStrip = false; };
                  };
                }];
              };
            })
          ];
        };

        backend = pkgs.backend.flake { };

        inherit (pkgs) lib stdenv;
      in rec {
        devShell = (pkgs.backend.shellFor {
          # Backend deps
          tools = {
            cabal.index-state = cabal-index-state;
            hoogle.index-state = cabal-index-state;
            haskell-language-server.index-state = cabal-index-state;
            cabal-fmt.index-state = cabal-index-state;
            ghcid.index-state = cabal-index-state;
            hlint.index-state = cabal-index-state;
          };

          buildInputs = with pkgs; [
            # Common deps
            git
            git-lfs
            heroku

            # Backend deps
            packages.backend-dev

            # Frontend deps
            nodejs
            nodePackages.pnpm
            packages.frontend-dev
            packages.frontend-watch
            packages.frontend-build
          ];
        });

        packages = {
          hoogle = pkgs.haskell-nix.hackage-package { };

          backend = backend.packages."backend:exe:server";

          frontend = pkgs.napalm.buildPackage ./frontend {
            installPhase = ''
              npm run build
              mkdir $out
              cp -ar dist/. $out
            '';
          };

          backend-dev = pkgs.writeShellScriptBin "backend-dev" ''
            STATIC_DIR=../frontend/dist ghcid -W -l -r --target server --restart src
          '';

          frontend-dev = pkgs.writeShellScriptBin "frontend-dev" ''
            pnpm dev
          '';
          frontend-watch = pkgs.writeShellScriptBin "frontend-watch" ''
            pnpm watch
          '';
          frontend-build = pkgs.writeShellScriptBin "frontend-build" ''
            pnpm build
          '';

          combined = stdenv.mkDerivation {
            pname = name;
            inherit version;
            buildInputs = [ pkgs.makeWrapper ];
            dontUnpack = true;
            installPhase = ''
              makeWrapper ${packages.backend}/bin/server $out/bin/${name} \
                --set STATIC_DIR ${packages.frontend}
            '';
          };

          docker = pkgs.dockerTools.buildLayeredImage {
            inherit name;
            tag = "latest";
            contents = with pkgs; [ busybox bash ];
            config = {
              Cmd = [ "${packages.backend}/bin/server" ];
              Env = [ "STATIC_DIR=${packages.frontend}" ];
            };
          };

          deploy = pkgs.writeShellScriptBin "deploy" ''
            docker load < ${packages.docker}
            docker tag ${name} registry.heroku.com/${name}/web
            docker push registry.heroku.com/${name}/web
          '';
        };

        defaultPackage = packages.combined;

        apps = {
          deploy = {
            type = "app";
            program = "${packages.deploy}/bin/deploy";
          };
        };

        defaultApp = apps.deploy;
      });
}
