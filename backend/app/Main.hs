{-# LANGUAGE FlexibleContexts #-}

module Main (main) where

import qualified "backend" Api
import qualified Api.ToDo as ToDo
import Control.Monad.Logger (NoLoggingT (..), runNoLoggingT)
import qualified "backend" Data
import qualified Data.ByteString.Char8 as BS
import Data.Maybe (fromMaybe)
import Database.Persist.Postgresql (createPostgresqlPool, runMigration, runSqlPool)
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Servant.Errors (errorMwDefJson)
import Options (Options (..))
import Servant
import Text.Read (readMaybe)
import UnliftIO (MonadUnliftIO, liftIO, withRunInIO)
import UnliftIO.Environment (lookupEnv)

putStrLn' :: MonadUnliftIO m => String -> m ()
putStrLn' = liftIO . putStrLn

runWarp :: MonadUnliftIO m => Warp.Port -> Application -> m ()
runWarp port warpApp = withRunInIO \_ -> Warp.run port warpApp

options :: (MonadUnliftIO m) => m Options
options = do
  databaseUrl <-
    fromMaybe "postgres://local:local@localhost:5432/local"
      <$> lookupEnv "DATABASE_URL"
  staticDirectory <- fromMaybe "static" <$> lookupEnv "STATIC_DIR"
  port <- fromMaybe 8080 . (readMaybe =<<) <$> lookupEnv "PORT"
  databaseConnection <- runNoLoggingT $ createPostgresqlPool (BS.pack databaseUrl) 8
  flip runSqlPool databaseConnection $ runMigration Data.migrateAll
  putStrLn' $ "Running with " <> show Options {..}
  pure Options {..}

api :: Options -> Server Api.Api
api Options {..} =
  ToDo.api Options {..} :<|> serveDirectoryFileServer staticDirectory

app :: (MonadUnliftIO m) => Options -> m ()
app Options {..} = runWarp port $ errorMwDefJson $ serve (Proxy @Api.Api) $ api Options {..}

main :: IO ()
main = options >>= app
