module Api.ToDo (api) where

import qualified Api.Crud as Crud
import "backend" Api.ToDo
import Options
import Servant

api :: Options -> Server Api
api = Crud.api
