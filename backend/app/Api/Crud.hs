module Api.Crud (api) where

import qualified "backend" Api.Crud as Crud
import Control.Monad.Reader
import Database.Persist
import Database.Persist.Postgresql
import Options
import Servant

api :: (PersistEntity a, PersistEntityBackend a ~ SqlBackend) => Options -> Server (Crud.Api a)
api Options {..} =
  runDb (selectList [] [])
    :<|> runDb . insert
    :<|> runDb . get
    :<|> runDb . delete
    :<|> (runDb .) . replace
  where
    runDb :: ReaderT SqlBackend IO a -> Handler a
    runDb = liftIO . flip runSqlPool databaseConnection
