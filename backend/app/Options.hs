module Options (Options (..)) where

import Database.Persist.Postgresql
import qualified Network.Wai.Handler.Warp as Warp

data Options = Options
  { databaseConnection :: ConnectionPool,
    staticDirectory :: FilePath,
    port :: Warp.Port
  }
  deriving (Show)
