module Api (Api) where

import qualified Api.ToDo as ToDo
import Servant

type Api =
  "api"
    :> ("todos" :> ToDo.Api)
    :<|> Raw
