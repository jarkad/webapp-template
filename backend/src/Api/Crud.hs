module Api.Crud (Api) where

import Database.Persist
import Servant

type Api a =
  Get '[JSON] [Entity a]
    :<|> ReqBody '[JSON] a :> Post '[JSON] (Key a)
    :<|> Capture "key" (Key a) :> Get '[JSON] (Maybe a)
    :<|> Capture "key" (Key a) :> Delete '[JSON] ()
    :<|> Capture "key" (Key a) :> ReqBody '[JSON] a :> Put '[JSON] ()
