module Api.ToDo (Api) where

import qualified Api.Crud as Crud
import Data

type Api = Crud.Api ToDo
