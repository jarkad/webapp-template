const HtmlPlugin = require('html-webpack-plugin')

const prod = process.env.NODE_ENV === 'production'

module.exports = {
  mode: prod ? 'production' : 'development',
  devtool: !prod && 'source-map',
  entry: './src/app.js',
  output: {clean: true},
  module: {
    rules: [
      {
        test: /\.svelte$/i,
        use: {
          loader: 'svelte-loader',
          options: {compilerOptions: {dev: !prod}, emitCss: prod},
        },
      },
      {
        test: /\.s(a|c)ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          {loader: 'sass-loader', options: {implementation: require('sass')}},
        ],
      },
      {test: /\.css$/i, use: ['style-loader', 'css-loader', 'postcss-loader']},
    ],
  },
  plugins: [
    new HtmlPlugin({
      minify: prod,
      meta: {
        viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no',
      },
    }),
  ],
}
